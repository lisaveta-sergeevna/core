﻿using System;

namespace CMSys.Web.ViewModels
{
    public class PageViewModel
    {
        public int PageNumber { get; set; }

        public int TotalPages { get; set; }

        public int PageSize { get; set; }

        public bool HasPreviousPage => (PageNumber > 1);

        public bool HasNextPage => (PageNumber < TotalPages);

        public PageViewModel(int totalEntries, int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;

            TotalPages = (int) Math.Ceiling((double) totalEntries / PageSize);
        }
    }
}
