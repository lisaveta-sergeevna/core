﻿using System.Collections.Generic;
using System.Linq;
using CMSys.Core.Entities.Catalog;

namespace CMSys.Web.ViewModels
{
    public class TrainerGroupViewModel
    {
        public TrainerGroup Group { get; set; }

        public List<Trainer> Trainers { get; set; }

        public TrainerGroupViewModel(IGrouping<TrainerGroup, Trainer> group)
        {
            Group = group.Key;
            Trainers = group.ToList();
        }
    }
}
