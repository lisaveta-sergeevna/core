﻿using System.Collections.Generic;
using CMSys.Core.Entities.Catalog;

namespace CMSys.Web.ViewModels
{
    public class CoursesViewModel
    {
        public IEnumerable<Course> Courses { get; set; }

        public PageViewModel PageViewModel { get; set; }

        public CoursesViewModel(IEnumerable<Course> courses, PageViewModel pageViewModel)
        {
            Courses = courses;
            PageViewModel = pageViewModel;
        }
    }
}
