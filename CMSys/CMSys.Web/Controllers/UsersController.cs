﻿using CMSys.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CMSys.Web.Controllers
{
    [Route("[controller]")]
    public class UsersController : Controller
    {
        private readonly ILogger<TrainersController> _logger;

        private readonly IUnitOfWork _unitOfWork;

        public UsersController(ILogger<TrainersController> logger,
            IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        [Route("[action]")]
        public IActionResult Users()
        {
            return View();
        }
    }
}
