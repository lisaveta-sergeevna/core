﻿using System.Linq;
using CMSys.Core.Repositories;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CMSys.Web.Controllers
{
    //[Route("[controller]")]
    public class CoursesController : Controller
    {
        private readonly ILogger<TrainersController> _logger;

        private readonly IUnitOfWork _unitOfWork;

        public CoursesController(ILogger<TrainersController> logger,
            IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        [Route("")]
        [Route("[action]")]
        public IActionResult Courses(int pageNumber = 1)
        {
            var courses = _unitOfWork.CourseRepository.All();
            var pageViewModel = new PageViewModel(courses.Count(), pageNumber, 5);
            var courseViewModel = new CoursesViewModel(courses, pageViewModel);

            return View(courseViewModel);
        }

        [Route("[action]")]
        public IActionResult CoursesList()
        {
            return View();
        }

        [Route("action")]
        public IActionResult CourseGroups()
        {
            return View();
        }
    }
}
