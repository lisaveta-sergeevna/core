﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CMSys.Core.Repositories;
using CMSys.Web.ViewModels;

namespace CMSys.Web.Controllers
{
    [Route("[controller]")]
    public class TrainersController : Controller
    {
        private readonly ILogger<TrainersController> _logger;

        private readonly IUnitOfWork _unitOfWork;

        public TrainersController(ILogger<TrainersController> logger,
            IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        [Route("[action]")]
        public IActionResult Trainers()
        {
            var groups = _unitOfWork.TrainerRepository
                .All()
                .GroupBy(trainer => trainer.TrainerGroup)
                .Select(group => new TrainerGroupViewModel(group));

            return View(groups);
        }

        [Route("[action]")]
        public IActionResult TrainersList()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult TrainerGroups()
        {
            return View();
        }
    }
}
